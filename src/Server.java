import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Ott Madis Ozolit
 *
 */
public class Server {
	/**
	 * Kaart, mis k�ikidele klientidele on �hine
	 */
	static Map map = new Map();
	/**
	 * M�ngijate arv, kes on serveriga �hendunud
	 */
	static int numOfPlayers = 0;
	/**
	 * Konstantne massiiv, mis hoiab m�ngijate unikaalseid s�mboleid.
	 */
	private static final char PLAYER_SYMBOLS[] = {'A','B','C','D'};	
	/**
	 * Port mille kaudu soklisse �hendada
	 */
	private static final int PORT = 1234;
	/**
	 * V�ljundvoogude massiiv/nimistu, mida saab kasutada 
	 * h�lpsasti k�ikidesse klientidesse korraga info saatmiseks
	 */
	private static ArrayList<ObjectOutputStream> outputStreams = new ArrayList<ObjectOutputStream>();
	/**
	 * Parasjagu aktiivsete m�ngijate massiiv/nimistu, 
	 * m�ngijad on t�histatud s�mboliga, mis on Character t��pi
	 */
	private static ArrayList<Character> playerList = new ArrayList<Character>();

	/**
	 * P�hifunktsioon. K�ivitab serveri ning hakkab klientide �hendussoove kuulama
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		System.out.println("Server up");
		ServerSocket listener = new ServerSocket(PORT);
		try {
			while (true) {
				if(numOfPlayers<4) {					
					new ClientHandler(listener.accept(), PLAYER_SYMBOLS[numOfPlayers]).start();
					playerList.add(PLAYER_SYMBOLS[numOfPlayers]);
					numOfPlayers++;
				}
			}
		} finally {
			listener.close();
		}
	}

	/**
	 * @author Ott Madis Ozolit
	 *
	 */
	private static class ClientHandler extends Thread {
		/**
		 * Kliendihalduri poolt hallatava kliendi s�mbol
		 */
		private char symbol = ' ';
		/**
		 * Sokkel kuhu klient �hendub
		 */
		private Socket socket;		
		private OutputStream os;
		/**
		 * Kliendihalduri v�ljundvoog, objektide saatmiseks kliendile
		 */
		private ObjectOutputStream oos;	
		private InputStream is;
		/**
		 * Kliendihalduri sisendvoog, objektide lugemiseks kliendilt
		 */
		private ObjectInputStream ois;
		/**
		 * Failikirjutaja
		 */
		private PrintWriter logger;
		/**
		 * Taimerobjekt, et lugeda aega j�rgmise r�nnakuk�suni
		 */
		private Timer timer = new Timer();
		/**
		 * T�ev��rtusmuutuja kontrollimaks, kas r�nnak on lubatud
		 */
		private boolean isTimeUp = true;
		/**
		 * T��k�sk, mis k�ivitatakse kui r�nnakutevaheline aeg on l�bi, sel juhul s�tib isTimeUp v��rtuse "true" peale
		 */
		private TurnTask turnTask = new TurnTask(timer, isTimeUp);

		/**
		 * RegEx, mis kontrollib r�ndelause s�ntaksit. Case insensitive "attack" ning 2 komadega eraldatud t�isarvu, omakorda sulgude vahel.
		 */
		Pattern attackPattern = Pattern.compile("^(?i)attack\\(\\d+,\\d+\\)");	
		/**
		 * RegEx, mis kontrollib t�isarvu olemasolu stringis
		 */
		Pattern intPattern = Pattern.compile("-?\\d+");

		/**
		 * Kontrollib attackPatterni abil sissetulevaid s�numeid, kas m�ni m�ngija tahab r�nnata
		 */
		Matcher attackMatcher;
		/**
		 * Otsib r�nnakuk�sust �les t�isarvud (koordinaadid)
		 */
		Matcher intMatcher;		
		/**
		 * Abimuutuja k�sustringide hoidmiseks
		 */
		String msgString;



		/**
		 * Kliendihalduri konstruktor
		 * 
		 * @param socket Sokkel, mille kaudu �henduda
		 * @param symbol M�ngija s�mbol, kes �hendus m�ngu
		 */
		public ClientHandler(Socket socket, char symbol) {
			this.socket = socket;
			this.symbol = symbol;
		}

		public void run() {
			try {
				/**
				 * Abimuutuja uue r�nnaku aja loendamiseks
				 */
				Long currentTime = System.nanoTime();
				os = socket.getOutputStream();  
				oos = new ObjectOutputStream(os);      	
				is = socket.getInputStream();  
				ois = new ObjectInputStream(is);
				logger = new PrintWriter("logger" + symbol + ".txt", "UTF-8");

				//�hendanud m�ngijale saadetakse talle m��ratud s�mbol ning praegune kaardiseis
				oos.writeObject("SYMBOL");
				oos.writeObject(symbol);                 
				oos.writeObject(map);
				logger.println(symbol + " joined game");
				printMap();
				//Lisame v�ljundvoogude nimistusse uue m�ngija v�ljundvoo
				outputStreams.add(oos);

				//Kliendihaldur kuulab pidevalt kliendilt tulevaid s�numeid
				while (true) {
					String input = null;
					//loeme seni infot, kuni sokkel on lahti
					if(!socket.isClosed()) {
						input = (String)ois.readObject();
					}
					//Kui sisend on null, returnime run funktsioonist v�lja
					if (input == null) {
						return;
					}
					attackMatcher = attackPattern.matcher(input);
					if(numOfPlayers < 2) {
						msgString = "MESSAGE not enough players to play";
						logger.println(msgString);
						oos.writeObject(msgString);
					}
					else if(attackMatcher.find()) {						
						if(turnTask.isTimeUp()) {
							currentTime = System.nanoTime();
							timer = new Timer();
							timer.schedule(turnTask = new TurnTask(timer, false), 10000);			            	

							int i=0,j=0;
							intMatcher = intPattern.matcher(input);							
							intMatcher.find();
							i=Integer.parseInt(intMatcher.group());
							intMatcher.find();
							j=Integer.parseInt(intMatcher.group());
							input = input.concat(" coord x: " + i + " coord y: " + j);
							//Ei tohi maatriksi piiridest v�lja r�nnata
							if (i>3 || j>3 || i<0 || j<0) {
								msgString = "MESSAGE You can't attack outside given territory: " + symbol + ": " + input;
								logger.println(msgString);
								oos.writeObject(msgString);
							}
							//Oma territooriumi ei tohi r�nnata
							else if(map.map[i][j] == symbol) {
								msgString = "MESSAGE You can't attack your own territory: " + symbol + ": " + input;
								logger.println(msgString);
								oos.writeObject(msgString);
							}						
							//Kui koordinaadid on legaalsed, vallutab riik selle ala omale
							else {								
								map.map[i][j] = symbol;							
								Map tempMap = fillMap();
								printMap();
								//kontrollime kas p�rast r�nnakut m�ng on l�bi, kui jah, k�ime k�ik
								//kliendid l�bi ja saadame neile k�igile GAMEOVER teate
								if(checkEnd()) {
									msgString = "GAMEOVER " + symbol + ": won the game with move " + input;
									for (ObjectOutputStream writer : outputStreams) {											
										logger.println(msgString);
										writer.writeObject(msgString);
										writer.writeObject(tempMap);
									}									
									System.out.println("Game over.");		
									break;
								}
								//kui m�ng pole l�bi, saadame k�ikidele klientidele uuendatud kaardiseisu p�rast r�nnakut
								else {
									msgString = "MAPUPDATE " + symbol + ": " + input;
									for (ObjectOutputStream writer : outputStreams) {
										writer.writeObject(msgString);
										writer.writeObject(tempMap);
									}
									logger.println(msgString);
									printMap();
								}
							}
						}
						//Kui vajalik aeg r�nnakute vahel pole m��dunud, kuvame kliendile, et ta peab veel ootama 
						else {
							msgString = "MESSAGE You need to wait " + (10 - TimeUnit.NANOSECONDS.toSeconds((System.nanoTime() - currentTime))) + " seconds to attack again.";
							logger.println(msgString);
							oos.writeObject(msgString);
						}
					}
					//Kui klient tr�kib "playerlist", kuvame aktiivsete m�ngijate nimekirja
					else if(input.equals("playerlist")) {
						msgString = "MESSAGE Players active: ";
						for(Character symbol : playerList) {
							msgString = msgString.concat(" " + symbol);							
						}
						logger.println(msgString);
						oos.writeObject(msgString);
					}
					//Tavas�numi kuvamine
					else {
						msgString = "MESSAGE " + symbol + ": " + input;
						for (ObjectOutputStream writer : outputStreams) {
							writer.writeObject(msgString);
						}
						logger.println(msgString);
					}
				}
			} catch (IOException e) {
				if(e.getClass().equals(SocketException.class)) {
					logger.close();
					outputStreams.remove(oos);
					playerList.remove((Character)symbol);
					for (ObjectOutputStream writer : outputStreams) {
						try {
							writer.writeObject("MESSAGE " + symbol + ": disconnected");							
						} catch (IOException e1) {
							System.out.println(e1.getMessage());							
						}
					}
					System.out.println(e.getMessage());
					return;
				}
				System.out.println(e.getMessage());
			} catch (ClassNotFoundException e) {
				System.out.println(e.getMessage());
			} finally {
				if (oos != null) {
					outputStreams.remove(oos);
				}
				try {
					socket.close();
				} catch (IOException e) {
					System.out.println(e.getMessage());
				}
			}
		}

		/**
		 * Kontrollib kaarti - kui k�ik kaardi elemendid on
		 * vaid �he s�mboli poolt kaetud, on m�ng l�bi.
		 * 
		 * @return Tagastab t�ev��rtuse, kas m�ng on l�bi v�i mitte
		 */
		private boolean checkEnd() {
			//Eeldame, et m�ng on l�bi
			boolean isOver = true;
			//K�ime l�bi kaardi k�ikide s�mbolite vastu
			for(char symbol : PLAYER_SYMBOLS) {
				//Eeldame iga eri s�mboli korral, et m�ng on l�bi
				isOver = true;
				for(int i=0; i<4; i++) {
					for(int j=0; j<4; j++) {
						//M�ng ei ole l�ppenud, kui v�hemalt �ks kaardiala ei kattu kliendi s�mboliga
						if(map.map[i][j] != symbol) {
							isOver = false;
						}
					}
					System.out.println("");
				}
				//Kui mingi s�mboli puhul m�ng on l�bi, l�petame kontrollits�kli
				if(isOver) break;
			}        	
			return isOver;
		}

		/**
		 * Tr�kib kaardi konsooli ja logifaili
		 */
		private void printMap() {
			for(int i=0; i<4; i++) {
				for(int j=0; j<4; j++) {
					System.out.print(map.map[i][j]);
					logger.print(map.map[i][j]);
				}
				System.out.println("");
				logger.println("");
			}
		}

		/**
		 * T�idab uue ajutise kaardiobjekti ning tagastab selle
		 * klientidele edasisaatmiseks.
		 * 
		 * @return Tagastab t�idetud kaardi
		 */
		private Map fillMap() {
			Map tempMap = new Map();
			for(int i=0; i<4; i++) {
				for(int j=0; j<4; j++) {
					tempMap.map[i][j] = map.map[i][j];
				}
			}
			return tempMap;
		}
	}
}