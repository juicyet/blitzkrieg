import java.io.Serializable;

/**
 * @author Ott Madis Ozolit
 *
 */
public class Map implements Serializable {
	public static final int MATRIX_DIMENSION = 4;

	/**
	 * version ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Kaardimassiiv, on 4x4 char massiiv vaikimisi.
	 */
	char[][] map = new char [MATRIX_DIMENSION][MATRIX_DIMENSION];	

	/**
	 * Kaardiobjekti konstruktor
	 */
	public Map() {
		super();
		initMap();
	}

	/**
	 * Initsialiseerib kaardiobjekti vaikimis v��rtustega - igal m�ngijal on 4 ruutu algseisus.
	 */
	public void initMap () {
		map[0][0] = 'A';
		map[0][1] = 'A';
		map[1][0] = 'A';
		map[1][1] = 'A';

		map[2][0] = 'B';
		map[3][0] = 'B';
		map[2][1] = 'B';
		map[3][1] = 'B';

		map[0][2] = 'C';
		map[0][3] = 'C';
		map[1][2] = 'C';
		map[1][3] = 'C';

		map[2][2] = 'D';
		map[2][3] = 'D';
		map[3][2] = 'D';
		map[3][3] = 'D';		
	}
}
