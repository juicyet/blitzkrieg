import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * @author Ott Madis Ozolit
 *
 */
public class Client {
	/**
	 * Port mille kaudu soklisse �hendada
	 */
	private static final int PORT = 1234;
	/**
	 * Kliendis hoitav hetkeseisuga kaardiobjekt
	 */
	Map map;
	OutputStream os;
	/**
	 *  V�ljundvoog objektide serverile saatmiseks
	 */
	ObjectOutputStream oos;	
	InputStream is;
	/**
	 * Sisendvoog objektide serverilt lugemiseks
	 */
	ObjectInputStream ois;
	/**
	 * M�ngija unikaalne s�mbol m�ngus (v�ljakul ja m�ngijate nimekirjas)
	 */
	char playerSymbol;

	/**
	 * GUI ehitamine.
	 * Koosneb frame'st, kus on tekstiv�li ja logimiseks m�eldud tekstiala
	 */
	JFrame frame = new JFrame("Pela");
	JTextField userTextInput = new JTextField(40);
	JTextArea userLog = new JTextArea(8, 40);

	/**
	 * Kliendi klassi konstruktor
	 */
	public Client() {
		userTextInput.setEditable(false);
		userLog.setEditable(false);
		frame.getContentPane().add(userTextInput, "North");
		frame.getContentPane().add(new JScrollPane(userLog), "Center");
		frame.pack();

		userTextInput.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					oos.writeObject(userTextInput.getText());
				} catch (IOException e1) {
					System.out.println(e1.getMessage());
					userLog.append("\n" + e1.getMessage());
				}
				userTextInput.setText("");
			}
		});
	}

	/**
	 * K�sib kasutajalt serveri aadressi kasutajaliidese kaudu
	 * 
	 * @return Tagastab serveriaadressi
	 */
	private String getServerAddress() {
		return JOptionPane.showInputDialog(
				frame,
				"Sisesta serveri aadress:",
				"Pela",
				JOptionPane.QUESTION_MESSAGE);
	}

	/**
	 * Paneb kliendi k�ima - k�sib serveri aadressi, avab sokli 
	 * ning hakkab serverit kuulama ning lubab serveriga suhelda.
	 * 
	 * Serverilt saadud infot kontrollitakse esmalt kontrolls�na j�rgi:
	 * MESSAGE: teksts�num/teavitus
	 * MAPUDPATE: kaardile on tekkinud uuendusi
	 * GAMEOVER: m�ng l�ppes
	 * 
	 * @throws IOException
	 */
	private void run() throws IOException {
		String serverAddress = getServerAddress();
		Socket socket = new Socket(serverAddress, PORT);

		os = socket.getOutputStream();  
		oos = new ObjectOutputStream(os);      	
		is = socket.getInputStream();  
		ois = new ObjectInputStream(is);

		while (true) {
			String line;
			try {
				line = (String)ois.readObject();
				if (line.startsWith("SYMBOL")) {
					playerSymbol = (char)ois.readObject();
					userTextInput.setEditable(true);
					map = (Map)ois.readObject();
					userLog.append("I am player " + playerSymbol + "\n");
					printMap();
				}
				else if (line.startsWith("MESSAGE")) {
					userLog.append(line.substring(8) + "\n");
				}
				else if (line.startsWith("MAPUPDATE")) {
					map = (Map)ois.readObject();
					printMap();
					userLog.append(line.substring(10) + "\n");	            	
				}
				else if (line.startsWith("GAMEOVER")) {
					map = (Map)ois.readObject();
					printMap();
					userLog.append(line.substring(9) + "\n");
					userTextInput.setEnabled(false);
					break;
				}
			} catch (ClassNotFoundException | EOFException e) {
				socket.close();
				userTextInput.setEnabled(false);
				userLog.append("Fatal error\n");
				break;
			} catch (SocketException e) {
				socket.close();
				userTextInput.setEnabled(false);
				userLog.append("Connection to server lost\n");
				break;
			}
		}
		socket.close();
	}

	/**
	 * P�hifunktsioon. Jooksutab kliendi GUI't.
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		Client client = new Client();
		client.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		client.frame.setVisible(true);
		client.run();
	}

	/**
	 * Tr�kib kaardi kasutajalogisse
	 */
	public void printMap() {
		for(int i=0; i<4; i++) {
			for(int j=0; j<4; j++) {
				userLog.append(Character.toString(map.map[i][j]));							
			}
			userLog.append("\n");
		}
		userLog.append("\n");
	}
}