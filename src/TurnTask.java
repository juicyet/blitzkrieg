import java.util.Timer;
import java.util.TimerTask;

/**
 * @author Ott Madis Ozolit
 *
 */
class TurnTask extends TimerTask {
	private Timer timer;
	private boolean isTimeUp;

	public void run() {
		isTimeUp = true;
		timer.cancel(); 
	}
	/**
	 * @param timer Timer-objekt, mille loendamise l�ppedes sooritatakse TurnTaski k�sud
	 * @param isTimeUp T�ev��rtus, kas timeri poolt loendatud aeg on juba l�bi v�i mitte
	 */
	public TurnTask(Timer timer, boolean isTimeUp) {
		super();
		this.timer = timer;
		this.isTimeUp = isTimeUp;
	}
	public boolean isTimeUp() {
		return isTimeUp;
	}
	public void setTimeUp(boolean isTimeUp) {
		this.isTimeUp = isTimeUp;
	}
}